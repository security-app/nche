import 'package:flutter/material.dart';
import 'package:vigilante/constants/constants.dart';

class EmergencyContact {
  EmergencyContact({this.nv1, this.nv2, this.nv3});

  var nv1;
  var nv2;
  var nv3;
}

class AddEmergencyContact extends StatefulWidget {
  AddEmergencyContact({Key? key}) : super(key: key);

  @override
  _AddEmergencyContactState createState() => _AddEmergencyContactState();
}

class _AddEmergencyContactState extends State<AddEmergencyContact> {
  List<Map> contactsList = [];

  var nv1;
  var nv2;
  var nv3;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Emergency Contacts'),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconButton(
              onPressed: () {
                showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (BuildContext context) {
                      return SingleChildScrollView(
                        child: Container(
                          height: 300,
                          padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom,
                          ),
                          child: Column(
                            children: [
                              ElevatedButton(
                                child: Text(
                                  'Select Contact',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 25,
                                    color: kSecondaryColor,
                                  ),
                                ),
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            kPrimaryColorLighter),
                                    padding: MaterialStateProperty.all<
                                            EdgeInsetsGeometry>(
                                        EdgeInsets.all(10))),
                                onPressed: () {},
                              ),
                              SizedBox(height: 10),
                              Text(
                                'Or',
                                style: TextStyle(fontSize: 20),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 10),
                              EmergencyTextFields(
                                hintText: 'Fullname',
                                enabled: true,
                              ),
                              EmergencyTextFields(hintText: 'Number'),
                              EmergencyTextFields(hintText: 'Relationship'),
                            ],
                          ),
                        ),
                      );
                    });
              },
              icon: Icon(Icons.add),
            ),
          ),
        ],
      ),
      body: contactsList.isEmpty
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('No emergency contact added'),
                ],
              ),
            )
          : Column(),
    );
  }
}

class EmergencyTextFields extends StatefulWidget {
  EmergencyTextFields(
      {Key? key, required this.hintText, this.newValue, this.enabled});

  final String hintText;
  final enabled;
  var newValue;

  @override
  _EmergencyTextFieldsState createState() => _EmergencyTextFieldsState();
}

class _EmergencyTextFieldsState extends State<EmergencyTextFields> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      enabled: widget.enabled,
      style: TextStyle(color: kSecondaryColor),
      textAlign: TextAlign.center,
      decoration: InputDecoration(
        hintText: widget.hintText,
        filled: true,
      ),
      onChanged: (value) {
        setState(() {
          widget.newValue = value;
        });
      },
    );
  }
}

