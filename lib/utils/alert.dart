import 'package:flutter/material.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:vigilante/constants/constants.dart';

class AlertBox extends StatefulWidget {
  AlertBox({Key? key}) : super(key: key);

  @override
  _AlertBoxState createState() => _AlertBoxState();
}

class _AlertBoxState extends State<AlertBox> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: ElevatedButton(
          onPressed: () {
            CoolAlert.show(
              context: context,
              type: CoolAlertType.error,
              text: "There was an error processing your transaction",
              title: '',
              backgroundColor: kSecondaryColorLighter,
              confirmBtnColor: kPrimaryColor,
            );
          },
          child: Text('Click Me'),
        ),
      ),
    );
  }
}
