import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:vigilante/constants/constants.dart';

class AppButton extends StatefulWidget {
  AppButton({
    Key? key,
    required this.title,
    this.verticalPadding = 20,
    this.horizontalPadding = 0,
    this.onPressed,
  }) : super(key: key);

  final String title;
  final double verticalPadding;
  final double horizontalPadding;
  final VoidCallback? onPressed;

  @override
  _AppButtonState createState() => _AppButtonState();
}

class _AppButtonState extends State<AppButton> {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: widget.onPressed,
      child: Text(
        widget.title,
        style: TextStyle(color: kPrimaryColor),
      ),
      style: ButtonStyle(
        padding:
            MaterialStateProperty.all<EdgeInsetsGeometry>(EdgeInsets.symmetric(
          vertical: widget.verticalPadding,
          horizontal: widget.horizontalPadding,
        )),
        backgroundColor: MaterialStateProperty.all<Color>(kSecondaryColor),
      ),
    );
  }
}
