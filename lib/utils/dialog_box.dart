import 'package:flutter/material.dart';

class DialogBox {
  DialogBox({required this.title, required this.content, required this.onPressed});

  late String title;
  late String content;
  late VoidCallback onPressed;

  Future<dynamic> dialogBox(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: [
            TextButton(
              onPressed: onPressed,
              child: Text('Yes'),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('No'),
            ),
          ],
        );
      },
    );
  }
}
