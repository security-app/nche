import 'package:flutter/material.dart';
import 'package:vigilante/constants/constants.dart';
import 'package:vigilante/utils/buttons.dart';

class DeleteAccount extends StatefulWidget {
  DeleteAccount({Key? key}) : super(key: key);

  @override
  _DeleteAccountState createState() => _DeleteAccountState();
}

class _DeleteAccountState extends State<DeleteAccount> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Delete account'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: Text(
                'Deleting your account will erase all your information from our database.',
                textAlign: TextAlign.center,
                style: kPrimaryTextStyle,
              ),
            ),
            SizedBox(height: 20),
            Text(
              'This cannot be undone.',
              textAlign: TextAlign.center,
              style: kPrimaryTextStyle,
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppButton(
                  title: 'Cancel',
                  horizontalPadding: 40,
                  verticalPadding: 15,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                SizedBox(width: 20),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    'Delete Account',
                    style: TextStyle(color: kDangerColor),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
