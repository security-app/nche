import 'package:flutter/material.dart';
import 'package:vigilante/constants/constants.dart';

class ChangePassword extends StatefulWidget {
  ChangePassword({Key? key}) : super(key: key);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Change Password'),
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.all(16),
            child: Stack(
              children: [
                Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Text(
                          'Enter current password:',
                          style: kPrimaryTextStyle,
                        ),
                      ),
                      Container(
                        child: TextField(
                          obscureText: true,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          'Enter new password:',
                          style: kPrimaryTextStyle,
                        ),
                        padding: EdgeInsets.symmetric(vertical: 20),
                      ),
                      Container(
                          child: TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        ),
                      )),
                      Container(
                        child: Text(
                          'Confirm new password:',
                          style: kPrimaryTextStyle,
                        ),
                        padding: EdgeInsets.symmetric(vertical: 20),
                      ),
                      Container(
                          child: TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        ),
                      )),
                      SizedBox(height: 30),
                      ElevatedButton(
                        onPressed: () {},
                        child: Text('Save'),
                        style: ButtonStyle(
                            padding:
                                MaterialStateProperty.all<EdgeInsetsGeometry>(
                                    EdgeInsets.all(20))),
                      ),
                    ]),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
