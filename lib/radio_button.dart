import 'package:flutter/material.dart';

enum BestTutorSite { male, female }

class RadioButton extends StatefulWidget {
  // RadioButton({Key: key}) : super(key: key);

  @override
  _RadioButtonState createState() => _RadioButtonState();
}

class _RadioButtonState extends State<RadioButton> {
  BestTutorSite _site = BestTutorSite.male;
  
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: ListTile(
              title: const Text('Male'),
              leading: Radio(
                value: BestTutorSite.male,
                groupValue: _site,
                onChanged: (BestTutorSite? value) {
                  setState(() {
                    _site = value!;
                  });
                },
              ),  
            ),
          ),  
          Expanded(
            child: ListTile(  
              title: const Text('Female'),
              leading: Radio(  
                value: BestTutorSite.female,
                groupValue: _site,  
                onChanged: (BestTutorSite? value) {  
                  setState(() {  
                    _site = value!; 
                  });  
                },  
              ),  
            ),
          ),  
        ],  
      ),
    );  
  }  
}  