import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:vigilante/constants/constants.dart';

class UserMapLocation extends StatefulWidget {
  UserMapLocation({Key? key}) : super(key: key);

  @override
  _UserMapLocationState createState() => _UserMapLocationState();
}

class _UserMapLocationState extends State<UserMapLocation> {
  Completer<GoogleMapController> _controller = Completer();

  String? dropdownValue = 'Burglary';

  List<String> items = ['Burglary', 'Assault', 'Fire', 'Accident', 'Medical'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Show Ruth\'s location'),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.person),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.notifications),
          ),
        ],
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: GoogleMap(
                myLocationButtonEnabled: true,
                trafficEnabled: true,
                myLocationEnabled: true,
                zoomControlsEnabled: true,
                mapType: MapType.normal,
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                },
                initialCameraPosition: CameraPosition(
                  target: LatLng(6.444550, 7.490180),
                  zoom: 14.4746,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) => AddEmergencyContact(),
                //   ),
                // );

                showModalBottomSheet(
                    context: context,
                    builder: (context) {
                      return Column(
                        children: [
                          DropdownButton<String>(
                            value: dropdownValue,
                            items: items.map((String item) {
                              return DropdownMenuItem<String>(
                                child: Text(item),
                                value: item,
                              );
                            }).toList(),
                            onChanged: (newValue) {
                              setState(() {
                                dropdownValue = newValue;
                              }); 
                            },
                          )
                        ],
                      );
                    });
              },
              child: Text(
                'Routes Page',
                style: TextStyle(color: kSecondaryColor),
              ),
            )
          ],
        ),
      ),
    );
  }
}
