import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:vigilante/bottomNav_Pages/bottom_navigation.dart';
import 'package:vigilante/bottomNav_Pages/home_page.dart';
import 'package:vigilante/constants/constants.dart';
import 'package:vigilante/screens/Phone_input_page.dart';
import 'package:vigilante/screens/intro_Page.dart';
import 'package:vigilante/screens/phone_verification_page.dart';
import 'package:vigilante/screens/signIn_page.dart';
import 'package:vigilante/screens/signUp_page.dart';
import 'package:vigilante/screens/signin_signup_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData().copyWith(
          accentColor: kPrimaryColor,
          bottomNavigationBarTheme: BottomNavigationBarThemeData(
            unselectedItemColor: kSecondaryColor,
          ),
          appBarTheme: AppBarTheme(
            backgroundColor: kSecondaryColor,
          ),
        ),
        debugShowCheckedModeBanner: false,
        initialRoute: IntroPage.id,
        routes: {
          IntroPage.id: (context) => IntroPage(),
          SignInPage.id: (context) => SignInPage(),
          PhoneInputPage.id: (context) => PhoneInputPage(),
          PhoneVerificationPage.id: (context) => PhoneVerificationPage(),
          SignUpPage.id: (context) => SignUpPage(),
          HomePage.id: (context) => HomePage(),
          MyBottomNavigation.id: (context) => MyBottomNavigation(),
          SignInSignUpPage.id: (context) => SignInSignUpPage(),
        });
  }
}





















// import 'package:flutter/material.dart';
// import 'package:vigilante/constants/constants.dart';
// import 'package:vigilante/screens/home.dart';
// import 'package:firebase_core/firebase_core.dart';

// void main() async {
//   WidgetsFlutterBinding.ensureInitialized();
//   await Firebase.initializeApp();
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       theme: ThemeData().copyWith(
//         accentColor: kPrimaryColor,
//         bottomNavigationBarTheme: BottomNavigationBarThemeData(
//           unselectedItemColor: kSecondaryColor,
//         ),
//         appBarTheme: AppBarTheme(
//           backgroundColor: kSecondaryColor,
//         ),
//       ),
//       home: HomePage(),
//       debugShowCheckedModeBanner: false,
//     );
//   }
// }
