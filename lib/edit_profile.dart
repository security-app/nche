import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:vigilante/constants/constants.dart';
import 'package:vigilante/radio_button.dart';
import 'package:vigilante/utils/buttons.dart';

final Stream<QuerySnapshot> collect =
    FirebaseFirestore.instance.collection('user1').snapshots();

TextEditingController textController = TextEditingController();
TextEditingController emailController = TextEditingController();
TextEditingController phoneController = TextEditingController();
TextEditingController locationController = TextEditingController();
TextEditingController twitterController = TextEditingController();

class EditProfile extends StatefulWidget {
  EditProfile({Key? key}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile'),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: collect,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text('Something went wrong'));
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: Column(
                children: [
                  CircularProgressIndicator(
                    color: kSecondaryColor,
                  ),
                  Text('Preparing your profile')
                ],
              ),
            );
          }

          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;

              // return ProfileTextField(
              //   hintText: data['fullname'],
              // );

              return SizedBox(
                height: 2000,
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  children: [
                    Center(
                      child: Stack(
                        children: [
                          CircleAvatar(
                            radius: 50,
                            backgroundImage: NetworkImage(
                              'https://cdn.pixabay.com/photo/2014/07/09/10/04/man-388104_960_720.jpg',
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            left: 65,
                            child: Icon(
                              Icons.photo_camera,
                              size: 30,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    ProfileTextField(
                      iconData: Icons.person,
                      controller: textController,
                      label: data['fullname'],
                      hintText: 'your fullname',
                    ),
                    SizedBox(height: 15),
                    ProfileTextField(
                      iconData: Icons.phone,
                      controller: phoneController,
                      label: data['phone'],
                      hintText: 'phone number',
                    ),
                    SizedBox(height: 15),
                    ProfileTextField(
                      iconData: Icons.email,
                      controller: emailController,
                      label: data['email'],
                      hintText: 'email',
                    ),
                    SizedBox(height: 15),
                    Text('Gender:', style: TextStyle(fontSize: 15)),
                    RadioButton(),
                    SizedBox(height: 15),
                    ProfileTextField(
                      iconData: Icons.location_on,
                      controller: locationController,
                      label: data['location'],
                      hintText: 'your location',
                    ),
                    SizedBox(height: 15),
                    ProfileTextField(
                      iconData: FontAwesomeIcons.twitter,
                      controller: twitterController,
                      label: data['twitter'],
                      hintText: 'twitter profile link',
                    ),
                    SizedBox(height: 20),
                    AppButton(title: 'Save Changes')
                  ],
                ),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}

class ProfileTextField extends StatelessWidget {
  ProfileTextField(
      {Key? key, this.onChanged, this.iconData, this.controller, this.label, this.hintText})
      : super(key: key);

  late String? hintText;
  late IconData? iconData;
  late TextEditingController? controller;
  late String? label;
  var onChanged;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      onChanged: onChanged,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: label,
          hintText: hintText,
          prefixIcon: Icon(
            iconData,
            color: kSecondaryColor,
          ),
          focusedBorder:
              OutlineInputBorder(borderSide: BorderSide(color: kPrimaryColor))),
      // readOnly: true,
    );
  }
}
