import 'package:flutter/material.dart';
import 'package:vigilante/bottomNav_Pages/home/feed.dart';
import 'package:vigilante/components/Appbutton.dart';
import 'package:vigilante/components/SearchField.dart';
import 'package:vigilante/components/speeddial.dart';
import 'package:vigilante/demo.dart';
import 'package:vigilante/drawer/drawer_page.dart';
import 'package:vigilante/incident_report.dart';

var scaffoldKey = GlobalKey<ScaffoldState>();

class HomePage extends StatefulWidget {
  static const String id = 'HomePage';
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController;
  final _searchController = TextEditingController();

  String? dropdownValue;

  List<String> items = ['Burglary', 'Assault', 'Fire', 'Accident', 'Medical'];

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      drawer: DrawerPage(),
      drawerEnableOpenDragGesture: true,
      floatingActionButton: ButtonSpeedDial(),
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: () {
              scaffoldKey.currentState!.openDrawer();
            },
            child: Stack(
              children: [
                CircleAvatar(
                  radius: 50,
                  backgroundImage: NetworkImage(
                      'https://cdn.pixabay.com/photo/2014/07/09/10/04/man-388104_960_720.jpg'),
                ),
                Positioned(
                  bottom: 0,
                  right: 3,
                  child: CircleAvatar(
                    radius: 6,
                    backgroundColor: Colors.greenAccent[700],
                  ),
                ),
              ],
            ),
          ),
        ),
        centerTitle: true,
        title: Text('data'),
        backgroundColor: Colors.black54,
      ),
      backgroundColor: Colors.white,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              automaticallyImplyLeading: false,
              backwardsCompatibility: true,
              backgroundColor: Colors.white,
              toolbarHeight: 60.0,
              centerTitle: true,
              title: Padding(
                padding: const EdgeInsets.only(top: 5.0, bottom: 2.0),
                child: RoundedButton(
                  title: 'Report Incident',
                  color: Colors.black,
                  onpressed: () {
                    //TODO: Implement login functionality.

                    // showModalBottomSheet(
                    //     context: context,
                    //     builder: (BuildContext context) {
                    //       return Container(
                    //         padding: EdgeInsets.only(
                    //             bottom:
                    //                 MediaQuery.of(context).viewInsets.bottom),
                    //         child: Column(
                    //           children: [
                    //             DropdownButton<String>(
                    //                 value: dropdownValue,
                    //                 items: items.map<DropdownMenuItem<String>>(
                    //                     (String item) {
                    //                   return DropdownMenuItem<String>(
                    //                     value: item,
                    //                     child: Text(item),
                    //                   );
                    //                 }).toList(),
                    //                 onChanged: (newValue) {
                    //                   setState(() {
                    //                     dropdownValue = newValue;
                    //                   });
                    //                 })
                    //           ],
                    //         ),
                    //       );
                    //     });

                    Navigator.push(context, MaterialPageRoute(builder: (context) {
                      return IncidentReport();
                    }));
                  },
                ),
              ),
              pinned: true,
              floating: true,
              forceElevated: innerBoxIsScrolled,
              bottom: TabBar(
                indicatorColor: Colors.orange,
                unselectedLabelColor: Colors.black,
                labelColor: Colors.yellow[800],
                labelStyle:
                    TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                tabs: <Tab>[
                  Tab(text: 'My Feed'),
                  Tab(text: 'Popular'),
                  Tab(text: 'Favourite')
                ],
                controller: _tabController,
              ),
            ),
          ];
        },
        body: TabBarView(
          controller: _tabController,
          children: <Widget>[
            DemoPage(),
            Feed(),
            Feed(),
          ],
        ),
      ),
    );
  }
}
