import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:vigilante/constants/constants.dart';

final Stream<QuerySnapshot> collect =
    FirebaseFirestore.instance.collection('collect').snapshots();

class DemoPage extends StatefulWidget {
  DemoPage({Key? key}) : super(key: key);

  @override
  _DemoPageState createState() => _DemoPageState();
}

class _DemoPageState extends State<DemoPage> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: collect,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(color: kSecondaryColor,),
                SizedBox(height: 10),
                Text("Loading posts"),
              ],
            );
          }

          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;
              return Feeds(
                text: data['text'],
                upvotes: data['upvotes'],
                downvotes: data['downvotes'],
              );
            }).toList(),
          );
        });
  }
}

class Feeds extends StatelessWidget {
  const Feeds({Key? key, this.text, this.upvotes, this.downvotes})
      : super(key: key);

  final String? text;
  final int? upvotes;
  final int? downvotes;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text('$text'),
        Text('$upvotes'),
        Text('$downvotes'),
      ],
    );
  }
}
