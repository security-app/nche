import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:vigilante/constants/constants.dart';
import 'package:vigilante/drawer/settings_page.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:vigilante/profile.dart';
import 'package:vigilante/screens/signin_signup_page.dart';
import 'package:vigilante/utils/alert.dart';
import 'package:vigilante/utils/dialog_box.dart';

class DrawerPage extends StatelessWidget {
  const DrawerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: kSecondaryColorLighter,
        child: ListView(
          children: [
            SizedBox(height: 30),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Hero(
                    tag: 'userImage',
                    child: CircleAvatar(
                      radius: 30,
                      backgroundImage: NetworkImage(
                          'https://cdn.pixabay.com/photo/2014/07/09/10/04/man-388104_960_720.jpg'),
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    'Chimaobi Okolo',
                    style: TextStyle(color: Colors.white, fontSize: 17),
                  ),
                  SizedBox(height: 5),
                  Text(
                    'okolochimaobi@gmail.com',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 10,
                        fontStyle: FontStyle.italic),
                  ),
                  SizedBox(height: 5),
                  TextButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return ProfilePage();
                      }));
                    },
                    child: Text(
                      'My Profile',
                      style: TextStyle(color: kSecondaryColor, fontSize: 12),
                    ),
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(kPrimaryColor),
                        padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                            EdgeInsets.symmetric(horizontal: 20, vertical: 0))),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Divider(
              thickness: .5,
              color: kPrimaryColor,
            ),
            buildMenuItem(
                text: 'Rewards',
                icon: FontAwesomeIcons.moneyBillAlt,
                onTap: () {
                  Navigator.pop(context);
                }),
            buildMenuItem(
                text: 'Analytics', icon: Icons.analytics, onTap: () {}),
            buildMenuItem(
                text: 'Settings',
                icon: Icons.settings,
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return SettingsPage();
                  }));
                }),
            buildMenuItem(
                text: 'About', icon: FontAwesomeIcons.infoCircle, onTap: () {}),
            buildMenuItem(
              text: 'Logout',
              icon: Icons.logout,
              onTap: () {
                DialogBox(
                  title: 'Logout',
                  content: 'Are you sure?',
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) {
                      return SignInSignUpPage();
                    }));
                  },
                ).dialogBox(context);
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMenuItem(
      {required String text, required IconData icon, required onTap}) {
    final color = Colors.white;

    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: kPrimaryColor, width: .5))),
      child: ListTile(
        leading: FaIcon(
          icon,
          color: color,
        ),
        title: Text(
          text,
          style: TextStyle(color: color),
        ),
        onTap: onTap,
      ),
    );
  }
}
