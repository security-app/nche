import 'package:flutter/material.dart';
import 'package:vigilante/change_password.dart';
import 'package:vigilante/constants/constants.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:vigilante/delete_account.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  IconData iconData = Icons.toggle_off;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: ListView(
        children: [
          Container(
            child: Column(
              children: [
                ListTile(
                  title: Text('Account Settings'),
                  leading: FaIcon(FontAwesomeIcons.userCircle, color: kSecondaryColorLighter, semanticLabel: 'Account Settings',),
                ),
                Divider(
                  height: 10,
                  color: kSecondaryColor,
                ),
                ListTile(
                  title: Text('Change Password'),
                  trailing: Icon(Icons.arrow_right),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) {
                      return ChangePassword();
                    }));
                  },
                ),
                ListTile(
                  title: Text('Language'),
                  trailing: Icon(Icons.arrow_right),
                ),
                ListTile(
                  title: Text('View Saved Posts'),
                  trailing: Icon(Icons.arrow_right),
                ),
                ListTile(
                  title: Text('Delete Account', style: TextStyle(color: kDangerColor),),
                  trailing: Icon(Icons.arrow_right),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) {
                      return DeleteAccount();
                    }));
                  },
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Container(
            child: Column(
              children: [
                ListTile(title: Text('Display Settings'), leading: FaIcon(FontAwesomeIcons.mobileAlt, color: kSecondaryColorLighter, semanticLabel: 'Display Settings',),),
                Divider(
                  height: 10,
                  color: kSecondaryColor,
                ),
                ListTile(title: Text('Dark Mode'), trailing: Icon(iconData), onTap: () {
                  setState(() {
                    iconData = Icons.toggle_on;
                  });
                },),
              ],
            ),
          ),
          SizedBox(height: 20),
          Container(
            child: Column(
              children: [
                ListTile(title: Text('Notification Settings'), leading: FaIcon(FontAwesomeIcons.bell, color: kSecondaryColorLighter, semanticLabel: 'Notification Settings',),),
                Divider(height: 10, color: kSecondaryColor),
                ListTile(title: Text('Email and SMS Feedback'), trailing: Icon(Icons.toggle_off),),
                ListTile(title: Text('Auto-post to social media platforms'), trailing: Icon(Icons.toggle_off),),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
