import 'package:flutter/material.dart';
import 'package:vigilante/constants/constants.dart';
import 'package:vigilante/utils/buttons.dart';

class ReportUser extends StatefulWidget {
  ReportUser({Key? key}) : super(key: key);

  @override
  _ReportUserState createState() => _ReportUserState();
}

class _ReportUserState extends State<ReportUser> {
  static final List<String> items = <String>[
    'Select option',
    'Report user',
    'Report and block user',
  ];

  String? value = items.first;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Report User'),
        actions: [
          Padding(
            padding: EdgeInsets.all(8),
            child: IconButton(
              onPressed: () {},
              icon: Icon(Icons.close),
            ),
          )
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  child: DropdownButton<String>(
                    isExpanded: true,
                    value: value,
                    items: items
                        .map((item) => DropdownMenuItem<String>(
                              child: Text(item, style: kPrimaryTextStyle),
                              value: item,
                            ))
                        .toList(),
                    onChanged: (value) {
                      setState(() {
                        this.value = value;
                      });
                    },
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  'Title',
                  style: kPrimaryTextStyle,
                ),
                SizedBox(height: 10),
                TextField(
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Enter a title',
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  'Description',
                  style: kPrimaryTextStyle,
                ),
                SizedBox(height: 10),
                TextField(
                  textAlignVertical: TextAlignVertical.center,
                  textAlign: TextAlign.center,
                  maxLines: 10,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Enter description',
                  ),
                ),
                SizedBox(height: 20),
                AppButton(title: 'Submit Report',)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
