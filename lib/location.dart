import 'package:flutter/material.dart';
import 'package:vigilante/map_page.dart';
import 'package:vigilante/utils/buttons.dart';

class UserLocation extends StatefulWidget {
  UserLocation({Key? key}) : super(key: key);

  @override
  _UserLocationState createState() => _UserLocationState();
}

class _UserLocationState extends State<UserLocation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  decoration: BoxDecoration(
                    border: Border.all(),
                  ),
                  child: Column(
                    children: [
                      Text('Starting location'),
                      Text('Your current location'),
                      Divider(
                        height: 10,
                        color: Colors.black,
                      ),
                      Text('Destination location'),
                      Text('Some random address'),
                      SizedBox(height: 20),
                      AppButton(
                        title: 'Get Direction',
                        horizontalPadding: 20,
                        verticalPadding: 15,
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return UserMapLocation();
                          }));
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
