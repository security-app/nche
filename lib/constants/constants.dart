import 'package:flutter/material.dart';

// Colors

Color kPrimaryColor = Color(0XFFFFB400);
Color kPrimaryColorLighter = Color(0XFFFED579);
Color kSecondaryColor = Color(0XFF1c1c1c);
Color kSecondaryColorLighter = Color(0XFF242424);
Color kDangerColor = Color(0XFFFF0000);


// Font Style
TextStyle kPrimaryTextStyle = TextStyle(fontSize: 15);

const kbottomnavigationIconColor = Colors.black;

const kTextStyle = TextStyle(
  color: Colors.green,
  fontWeight: FontWeight.bold,
  fontSize: 30.0,
);

const kSendButtonTextStyle = TextStyle(
  color: Colors.lightBlueAccent,
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);

const kMessageTextFieldDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  hintText: 'Type your message here...',
  border: InputBorder.none,
);

const kMessageContainerDecoration = BoxDecoration(
  border: Border(
    top: BorderSide(color: Colors.lightBlueAccent, width: 2.0),
  ),
);

const kTextFieldDecoration = InputDecoration(
  hintStyle: TextStyle(color: Colors.black),
  hintText: 'Enter value',
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black, width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black, width: 2.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
);

// SizedBox kSizedBox({double height, double width, Widget child}) {
//   return SizedBox(
//     height: height,
//     width: height,
//     child: child,
//   );
// }