import 'package:flutter/material.dart';
import 'package:vigilante/bottomNav_Pages/home/feed.dart';
import 'package:vigilante/constants/constants.dart';
import 'package:vigilante/demo.dart';
import 'package:vigilante/edit_profile.dart';
import 'package:vigilante/utils/buttons.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
        centerTitle: true,
      ),
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              stretch: true,
              automaticallyImplyLeading: false,
              backwardsCompatibility: true,
              backgroundColor: Colors.white,
              toolbarHeight: 150.0,
              centerTitle: true,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Hero(
                          tag: 'userImage',
                          child: CircleAvatar(
                            radius: 40,
                            backgroundImage: NetworkImage(
                                'https://cdn.pixabay.com/photo/2014/07/09/10/04/man-388104_960_720.jpg'),
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          'Chimaobi Okolo',
                          style:
                              TextStyle(color: kSecondaryColor, fontSize: 17),
                        ),
                        SizedBox(height: 5),
                        Text(
                          'okolochimaobi@gmail.com',
                          style: TextStyle(
                              color: kSecondaryColor,
                              fontSize: 10,
                              fontStyle: FontStyle.italic),
                        ),
                      ],
                    ),
                  ),
                  AppButton(
                    title: 'Edit Profile',
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return EditProfile();
                        }),
                      );
                    },
                    horizontalPadding: 20,
                    verticalPadding: 15,
                  ),
                ],
              ),
              pinned: true,
              floating: true,
              forceElevated: innerBoxIsScrolled,
              bottom: TabBar(
                indicatorColor: Colors.orange,
                unselectedLabelColor: Colors.black,
                labelColor: Colors.yellow[800],
                labelStyle:
                    TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                tabs: <Tab>[
                  Tab(text: 'My posts'),
                  Tab(text: 'Interactions'),
                  Tab(text: 'Favourites')
                ],
                controller: _tabController,
              ),
            ),
          ];
        },
        body: TabBarView(
          controller: _tabController,
          children: <Widget>[
            DemoPage(),
            Feed(),
            Feed(),
          ],
        ),
      ),
    );
  }
}
