import 'package:flutter/material.dart';

// enum incidentReport { Burglary, Assault, Fire, Accident, Medical }

class IncidentReport extends StatefulWidget {
  IncidentReport({Key? key}) : super(key: key);

  @override
  _IncidentReportState createState() => _IncidentReportState();
}

class _IncidentReportState extends State<IncidentReport> {
  String dropdownValue = 'Burglary';

  List<String> items = ['Burglary', 'Assault', 'Fire', 'Accident', 'Medical'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Report Incident'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        children: [
          DropdownButton<String>(
            isExpanded: true,
            value: dropdownValue,
            items: items.map((String item) {
              return DropdownMenuItem<String>(
                child: Text(item),
                value: item,
              );
            }).toList(),
            onChanged: (newValue) {
              dropdownValue = newValue!;
            },
          ),
          SizedBox()
        ],
      ),
    );
  }
}
