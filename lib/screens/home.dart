import 'package:flutter/material.dart';
import 'package:vigilante/constants/constants.dart';
import 'package:vigilante/demo.dart';
import 'package:vigilante/drawer/drawer_page.dart';
import 'package:vigilante/location.dart';
import 'package:vigilante/report_user.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  static List screens = <Widget>[
    DemoPage(),
    UserLocation(),
    ReportUser(),
    Center(
      child: Text('SOS', style: TextStyle(fontSize: 30)),
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: DrawerPage(),
        appBar: AppBar(title: Text('HomePage'), centerTitle: true,),
        body: screens.elementAt(_selectedIndex),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedIndex,
          type: BottomNavigationBarType.fixed,
          onTap: _onItemTapped,
          selectedItemColor: Colors.black,
          selectedIconTheme: IconThemeData(color: kPrimaryColor),
          showUnselectedLabels: false,
          items: [
            bottomNavBarItem(icon: Icons.home_rounded, label: 'Home'),
            bottomNavBarItem(icon: Icons.notifications, label: 'Notifications'),
            bottomNavBarItem(
                icon: Icons.person_pin_rounded, label: 'Find a Friend'),
            bottomNavBarItem(
                icon: Icons.warning_rounded, label: 'SOS', color: kDangerColor),
          ],
        ),
      ),
    );
  }

  BottomNavigationBarItem bottomNavBarItem(
      {required IconData icon,
      required String label,
      Color color: Colors.white}) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: label,
      backgroundColor: color,
    );
  }
}
